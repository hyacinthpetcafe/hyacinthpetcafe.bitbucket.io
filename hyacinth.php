<?php
ini_set('sendmail_from', "smtp.gmail.com");
//print_r($_POST);
$firstname = htmlspecialchars(trim($_POST['firstname']));
$lastname = htmlspecialchars(trim($_POST['lastname']));
$email = trim($_POST['email']);
$phone = trim($_POST['phone']);
$comments = $_POST['comments'];
$header = "<h1>Thank You For Contacting Us!</h1>";
$enterStatement = "<h2>You Have Entered The Following Information:</h2>";
$nameText = "Name: " . $firstname . " " . $lastname;
$emailText = "Email: " . $email;
$phoneText = "Phone: " . $phone;
$commentsText = "Comments: " . $comments;
$subject = "Contact Us Form";
$msg = "$header\n\n" . "$enterStatement\n\n" . "$nameText<br><br>" . "$emailText<br><br>\n\n" . "$phoneText<br><br>\n\n" . "$commentsText<br><br>\n\n";
mail($email, $subject, $msg);

?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="final.css">
  <link rel="stylesheet" href="media.css">
</head>
<body>
<?php  

     echo "$header\n\n"; //displays <h1> text with double \n
     echo "$enterStatement\n\n"; //displays <h2> text with double \n
     echo "$nameText<br><br>\n\n"; // concatenated string and var for name with double <br> & \n
     echo "$emailText<br><br>\n\n"; //concatenated string and var for email with double <br> & \n
     echo "$phoneText<br><br>\n\n"; //concatenated string and var for phone with double <br> & \n
     echo "$commentsText<br><br>\n\n"; //concatenated string and var for comments with double <br> & \n

?>
</body>
</html>